# Facebook AudienceNetwork for Unity #

此專案測試/展示 **Unity 2019.4.10.f1** 串接 **audience-network-unity-sdk-6.2.0** 的使用流程

### AudienceNetwork相關服務啟用流程 ###

1. [創建Facebook帳號](https://business.facebook.com/pub/home/?business_id=977557079414652)並登入
2. 建立資產
3. 創建完畢後, 點選左邊功能列, 整合 -> 資產, 開啟自己剛剛建立的資產
4. 新增需要發布的平台
5. 另外建立廣告空間, 或使用預設的廣告空間(平台建立時即有)
6. 在廣告空間底下, 點選建立版位新增廣告項目
7. 後續整合串接會需要每個廣告項目的 **廣告版位編號**

### 廣告測試流程 ###

創建串接好廣告後須先註冊設定測試相關的設備才能看到廣告
點選左邊功能列, 整合 -> 測試, 啟用測試並新增測試裝置
> **註**: 需要輸入設備的IDFA or AAID以辨識

### Unity使用流程 ###

1. 解壓縮audience-network-unity-sdk-6.2.0.zip
2. 點擊unitypackage確認匯入檔案
3. 根據平台不同會有以下流程:
    | Android | iOS |
    | --- | --- |
    | Unity開始自動下載所需的SDK檔案, 輸出時會一起打包進apk中 | Unity不會自動下載相關函式庫, 當按下建置時才會下載最新版本的SDK。當開啟XCode時須特別注意點擊**xcworkspace**檔而非**xcodeproj**檔開啟 |

> **註**: xcworkspace檔大致上是使用到第三方套件的專案開啟, 沒有引用則點擊xcodeproj檔開啟即可

### 注意事項及常見問題 ###

1.廣告的Click相關callback觸發的時機是在顯示完廣告回到app時才會觸發, 並且會依據點了幾次觸發幾次, 跟UnityAds類似