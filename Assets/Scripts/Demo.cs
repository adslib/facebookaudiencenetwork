﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AudienceNetwork;

public class Demo : MonoBehaviour
{
    public Text adStatusText;

#if UNITY_ANDROID
    string bannerPlacementId = "356325752277715_371486090761681";
    string interstitialPlacementId = "356325752277715_356363755607248";
    string rewardedVideoPlacementId = "356325752277715_371554760754814";
#elif UNITY_IOS
    string bannerPlacementId = "";
    string interstitialPlacementId = "";
    string rewardedVideoPlacementId = "";
#endif

    private string debugTagPrefix = "=========";

    private bool isLoaded;
#pragma warning disable 0414
    private bool didClose;
#pragma warning restore 0414

    AdView adView; // Banner
    InterstitialAd interstitial;
    RewardedVideoAd rewardedVideo;

    void Awake()
    {
        AudienceNetworkAds.Initialize();
    }

    void OnDestroy()
    {
        // Dispose of banner ad when the scene is destroyed
        if (adView)
        {
            adView.Dispose();
        }

        if (interstitial)
        {
            interstitial.Dispose();
        }

        if (rewardedVideo)
        {
            rewardedVideo.Dispose();
        }
    }

    public void LoadBanner()
    {
        if (adView)
        {
            adView.Dispose();
        }

        adView = new AdView(bannerPlacementId, AdSize.BANNER_HEIGHT_90);
        adView.Register(gameObject);

        adView.AdViewDidLoad = AdViewDidLoad;
        adView.AdViewDidFailWithError = AdViewDidFailWithError;
        adView.AdViewWillLogImpression = AdViewWillLogImpression;
        adView.AdViewDidClick = AdViewDidClick;

        adView.LoadAd();
    }

    public void LoadInterstitial()
    {
        interstitial = new InterstitialAd(interstitialPlacementId);
        interstitial.Register(gameObject);

        interstitial.interstitialAdDidLoad = InterstitialAdDidLoad;
        interstitial.InterstitialAdDidFailWithError = InterstitialAdDidFailWithError;
        interstitial.InterstitialAdWillLogImpression = InterstitialAdWillLogImpression;
        interstitial.InterstitialAdDidClick = InterstitialAdDidClick;
        interstitial.InterstitialAdDidClose = InterstitialAdDidClose;

#if UNITY_ANDROID
        /*
         * Only relevant to Android.
         * This callback will only be triggered if the Interstitial activity has
         * been destroyed without being properly closed. This can happen if an
         * app with launchMode:singleTask (such as a Unity game) goes to
         * background and is then relaunched by tapping the icon.
         */
        interstitial.interstitialAdActivityDestroyed = InterstitialAdActivityDestroyed;
#endif

        interstitial.LoadAd();
    }

    public void LoadRewardedVideo()
    {
        rewardedVideo = new RewardedVideoAd(rewardedVideoPlacementId);

        // For S2S validation you can create the rewarded video ad with the reward data
        // Refer to documentation here:
        // https://developers.facebook.com/docs/audience-network/android/rewarded-video#server-side-reward-validation
        // https://developers.facebook.com/docs/audience-network/ios/rewarded-video#server-side-reward-validation
        RewardData rewardData = new RewardData
        {
            UserId = "1234",
            Currency = "0000"
        };

#pragma warning disable 0219
        RewardedVideoAd s2sRewardedVideoAd = new RewardedVideoAd(rewardedVideoPlacementId, rewardData);
#pragma warning restore 0219

        rewardedVideo.Register(gameObject);

        rewardedVideo.RewardedVideoAdDidLoad = RewardedVideoAdDidLoad;
        rewardedVideo.RewardedVideoAdDidFailWithError = RewardedVideoAdDidFailWithError;
        rewardedVideo.RewardedVideoAdWillLogImpression = RewardedVideoAdWillLogImpression;
        rewardedVideo.RewardedVideoAdDidClick = RewardedVideoAdDidClick;

        // For S2S validation you need to register the following two callback
        // Refer to documentation here:
        // https://developers.facebook.com/docs/audience-network/android/rewarded-video#server-side-reward-validation
        // https://developers.facebook.com/docs/audience-network/ios/rewarded-video#server-side-reward-validation
        rewardedVideo.RewardedVideoAdDidSucceed = RewardedVideoAdDidSucceed;

        rewardedVideo.RewardedVideoAdDidFail = RewardedVideoAdDidFail;
        rewardedVideo.RewardedVideoAdDidClose = RewardedVideoAdDidClose;

#if UNITY_ANDROID
        /*
         * Only relevant to Android.
         * This callback will only be triggered if the Rewarded Video activity
         * has been destroyed without being properly closed. This can happen if
         * an app with launchMode:singleTask (such as a Unity game) goes to
         * background and is then relaunched by tapping the icon.
         */
        rewardedVideo.RewardedVideoAdActivityDestroyed = RewardedVideoAdActivityDestroyed;
#endif

        rewardedVideo.LoadAd();
    }

    // AD事件 callback
    void AdViewDidLoad()
    {
        adView.Show(100);
        string isAdValid = adView.IsValid() ? "valid" : "invalid";
        adStatusText.text = "Banner loaded and is " + isAdValid + ".";
        Log("Banner loaded");
    }

    void AdViewDidFailWithError(string error)
    {
        adStatusText.text = "Banner failed to load with error: " + error;
        Log("Banner failed to load with error: " + error);
    }

    void AdViewWillLogImpression()
    {
        adStatusText.text = "Banner logged impression.";
        Log("Banner logged impression.");
    }
    void AdViewDidClick()
    {
        adStatusText.text = "Banner clicked.";
        Log("Banner clicked.");
    }

    void InterstitialAdDidLoad()
    {
        interstitial.Show();
        Log("Interstitial ad loaded.");
        isLoaded = true;
        didClose = false;
        string isAdValid = interstitial.IsValid() ? "valid" : "invalid";
        adStatusText.text = "Ad loaded and is " + isAdValid + ". Click show to present!";
    }

    void InterstitialAdDidFailWithError(string error)
    {
        Log("Interstitial ad failed to load with error: " + error);
        adStatusText.text = "Interstitial ad failed to load. Check console for details.";
    }

    void InterstitialAdWillLogImpression()
    {
        Log("Interstitial ad logged impression.");
    }

    void InterstitialAdDidClick()
    {
        Log("Interstitial ad clicked.");
    }

    void InterstitialAdDidClose()
    {
        Log("Interstitial ad did close.");
        didClose = true;
        if (interstitial != null)
        {
            interstitial.Dispose();
        }
    }

    void InterstitialAdActivityDestroyed()
    {
        if (!didClose)
        {
            Log("Interstitial activity destroyed without being closed first.");
            Log("Game should resume.");
        }
    }

    void RewardedVideoAdDidLoad()
    {
        rewardedVideo.Show();
        Log("RewardedVideo ad loaded.");
        isLoaded = true;
        didClose = false;
        string isAdValid = rewardedVideo.IsValid() ? "valid" : "invalid";
        adStatusText.text = "Ad loaded and is " + isAdValid + ". Click show to present!";
    }

    void RewardedVideoAdDidFailWithError(string error)
    {
        Log("RewardedVideo ad failed to load with error: " + error);
        adStatusText.text = "RewardedVideo ad failed to load. Check console for details.";
    }

    void RewardedVideoAdWillLogImpression()
    {
        Log("RewardedVideo ad logged impression.");
    }

    void RewardedVideoAdDidClick()
    {
        Log("RewardedVideo ad clicked.");
    }

    void RewardedVideoAdDidSucceed()
    {
        Log("Rewarded video ad validated by server");
    }

    void RewardedVideoAdDidFail()
    {
        Log("Rewarded video ad not validated, or no response from server");
    }

    void RewardedVideoAdDidClose()
    {
        Log("Rewarded video ad did close.");
        didClose = true;
        if (rewardedVideo != null)
        {
            rewardedVideo.Dispose();
        }
    }

    void RewardedVideoAdActivityDestroyed()
    {
        if (!didClose)
        {
            Log("Rewarded video activity destroyed without being closed first.");
            Log("Game should resume. User should not get a reward.");
        }
    }

    void Log(object msg)
    {
        Debug.Log(debugTagPrefix + " " + msg);
    }
}
